(ns kit.aws.core
  (:require
   [cognitect.aws.http.cognitect :as cog-http]
   [cognitect.aws.http]
   [cognitect.aws.client.api :as aws]
   [cognitect.aws.credentials :as creds]
   [cognitect.anomalies :as ca]
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   [kit.util :as u]))

(def ^:dynamic *credentials-provider* nil)

(defmacro with-credentials-provider [provider & body]
  `(binding [*credentials-provider* ~provider]
     ~@body))

(defn make-client [spec]
  (aws/client
   (merge {:http-client (cog-http/create)
           #_#_
           :credentials-provider (creds/cached-credentials-with-auto-refresh
                                  (creds/default-credentials-provider
                                   (cog-http/create)))}
          (when *credentials-provider*
            {:credentials-provider *credentials-provider*})
          spec)))

(def make-client-memo (memoize make-client))

(defn invoke
  ([client op]
   (invoke client op nil))
  ([client op request]
   (let [operation {:op (csk/->PascalCaseKeyword op)
                    :request (u/shallow-transform-keys
                              csk/->PascalCaseKeyword
                              request)}
         result (aws/invoke client operation)
         {anomaly ::ca/category} result]
     (when anomaly
       (throw (ex-info "AWS operation failed"
                       {:operation operation
                        :result result
                        :anomaly anomaly})))
     (u/shallow-transform-keys csk/->kebab-case-keyword result))))
